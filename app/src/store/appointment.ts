export type AppointmentType = {
  id: number,
  start: string,
  end?: string,
  duration: number
  position?: {
    width?: string,
    position?: number,
  },
  isOverlapWithPreviousAppointment?: boolean
  isOverlapWithNextAppointment?: boolean
}

function sortAppointments(appointments: AppointmentType[]){
  for (let i = 0; i < appointments.length; i++) {
    const startTime = appointments[i].start;
    const duration = appointments[i].duration;
    const [startHours, startMinutes] = startTime.split(":").map(Number);
    const end = new Date();
    end.setHours(startHours + Math.floor((startMinutes + duration) / 60));
    end.setMinutes((startMinutes + duration) % 60);
    const endHours = end.getHours().toString().padStart(2, "0");
    const endMinutes = end.getMinutes().toString().padStart(2, "0");
    appointments[i].start = `${startHours.toString().padStart(2, "0")}:${startMinutes.toString().padStart(2, "0")}`;
    appointments[i].end = `${endHours}:${endMinutes}`;
  }
  return appointments.sort((a, b) => a.start.localeCompare(b.start) || a.end.localeCompare(b.end));

}


function addPositionToAppointment(groups:Array<AppointmentType>[]) {
  groups.forEach(group => {
    if (group.length === 1) {
      group[0].position = {
        width: "full",
        position: 1,
      };
    } else if (group.length === 2) {
      const [first, second] = group;
      first.position = {
        width: `1/${group.length}`,
        position: 1,
      };
      second.position = {
        width: `1/${group.length}`,
        position: 2,
      };
    } else if (group.length === 3 || group.length === 4) {
      const [first, second, third, fourth] = group;

      if (fourth && fourth.start < third.end) {
        first.position = {
          width: `1/${group.length}`,
          position: 1,
        };
        second.position = {
          width: `1/${group.length}`,
          position: 2,
        };
        third.position = {
          width: `1/${group.length}`,
          position: 3,
        };
        fourth.position = {
          width: `1/${group.length}`,
          position: 4,
        };
      } else if (third && third.start < second.end) {
        first.position = {
          width: `1/${group.length - 1}`,
          position: 1,
        };
        second.position = {
          width: `1/${group.length - 1}`,
          position: 2,
        };
        third.position = {
          width: `1/${group.length - 1}`,
          position: 3,
        };
        if (fourth) {
          fourth.position = {
            width: `1/${group.length - 1}`,
            position: 1,
          };
        }
      } else {
        first.position = {
          width: `1/${group.length - 1}`,
          position: 1,
        };
        second.position = {
          width: `1/${group.length - 1}`,
          position: 2,
        };
        if (third) {
          third.position = {
            width: `1/${group.length - 1}`,
            position: 1,
          };
        }
        if (fourth) {
          fourth.position = {
            width: `1/${group.length - 1}`,
            position: 2,
          };
        }
      }
    }
  });
}



function combineArrays(...arrays: any[]) {
  let combinedArray:any = [];

  arrays.forEach((array) => {
    combinedArray = combinedArray.concat(array);
  });

  return combinedArray;
}

export function groupAppointments(appointments: AppointmentType[]) {

  const sortedAppointments = sortAppointments(appointments)

  const groups = [];
  let currentGroup = [sortedAppointments[0]];
  for (let i = 1; i < sortedAppointments.length; i++) {
    if ( sortedAppointments[i].start < currentGroup[currentGroup.length - 1].end) {
      currentGroup.push(sortedAppointments[i]);
    }
    else {
      groups.push(currentGroup);
      currentGroup = [sortedAppointments[i]];
    }
  }

  groups.push(currentGroup);
  console.log(groups)
  addPositionToAppointment(groups);
  return combineArrays(...groups);
}
