import {
  RecoilRoot,
} from 'recoil';
import Calendar from './components/calendar/Calendar';

function App() {

  return (
    <RecoilRoot>
      <div className="App">
        <Calendar
        />
      </div>
    </RecoilRoot>
  );
}

export default App;
