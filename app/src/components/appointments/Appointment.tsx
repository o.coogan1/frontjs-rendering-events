import { AppointmentType } from '../../store/appointment';
import { setVerticalPosition } from './helpers/AppointmentHelpers';

type AppointmentItemProps ={
  duration: AppointmentType['duration'],
  start: AppointmentType['start'],
  end: AppointmentType['end'],
  position: AppointmentType['position'],
}

function setPositioninGroup(position: AppointmentType['position']){
  switch(true){
    case position?.position === 1:
      return "left-0";
    case position?.position === 2 && position?.width === "1/2":
      return "left-1/2";
    case position?.position === 2 && position?.width === "1/3":
      return "left-1/3";
    case position?.position === 3 && position?.width === "1/3":
      return "left-2/3";
    case position?.position === 1 && position?.width === "1/4":
      return "left-0";
    case position?.position === 2 && position?.width === "1/4":
      return "left-1/4";
    case position?.position === 3 && position?.width === "1/4":
      return "left-2/4";
    case position?.position === 4 && position?.width === "1/4":
      return "left-3/4";
  }
}

function setWidthinGroup(position: AppointmentType['position']){
  switch(true){
    case position?.width === 'full':
      return "w-full";
    case position?.width === '1/2':
      return "w-1/2";
    case position?.width === '1/3':
      return "w-1/3";
    case position?.width === '1/4':
      return "w-1/4";
  }
}

export default function AppointmentItem({duration, start,end, position}: AppointmentItemProps){
  return (
    <div
      className={`border-l-4 border border-red-900 absolute bg-red-100 flex-row z-10 ${setWidthinGroup(position)} ${setPositioninGroup(position)}`}
      style={{top: `${setVerticalPosition(start)}rem`, height: `${duration * 5 / 60}rem`}}
    >
      <div className="items-center flex flex-row">
        <p
          className="text-red-900 text-xs"
        >
        Start-time: {start}
        </p>
      </div>
      <div className="items-center flex flex-row">
        <p
          className="text-red-900 text-xs"
        >
        End-time: {end}
        </p>
      </div>
    </div>
  );
}
