function setVerticalPosition(start: string){
  const [hour, minutes] = start.split(':');
  if(minutes) return (parseInt(hour) * 5 )+ (parseInt(minutes) * 5 / 60);
  return parseInt(hour) * 5 + parseInt(minutes) * 5;
}

function setAppointmentHeight(duration: number){
  return duration * 5 / 60
}


export {
  setVerticalPosition,
  setAppointmentHeight
}
