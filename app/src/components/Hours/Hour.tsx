export default function Hour({index}: {index: number}){
  return (
    <div
      className="h-20 border border-black"
    >
      <div
        className='w-20 h-20 border'
      >{`${index   }:00`}</div>
    </div>
  );
}
