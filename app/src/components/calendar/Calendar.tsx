import data from '../../data/input.json';
import AppointmentItem from '../appointments/Appointment';
import Hour from '../Hours/Hour';
import { AppointmentType, groupAppointments} from '../../store/appointment';
import { useState } from 'react';

export default function Calendar(){
  const hours = 24;
  const [appointmentList] = useState(groupAppointments(data));
  console.log(appointmentList)
  return (
    <div className="Calendar grid-cols-1 w-3/4 mx-auto relative">
      {
        [...Array(hours)].map((_, index) => <Hour
          index={index}
        />)
      }
      {
        appointmentList.map((appointment: AppointmentType) => <AppointmentItem
          duration={appointment.duration}
          start={appointment.start}
          end={appointment.end}
          position={appointment.position}
        />)
      }
    </div>
  );
}
