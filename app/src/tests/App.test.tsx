/* eslint-disable radix */
const meetingList= [
  {
    'id': 11,
    'start': '19:00',
    'duration': 60
  },
  {
    'id': 9,
    'start': '10:00',
    'duration': 30
  },
  {
    'id': 10,
    'start': '11:50',
    'duration': 20
  },
  {
    'id': 11,
    'start': '12:00',
    'duration': 20
  },
];


const sortedMeetings = meetingList.sort((a, b) => {
  const [aHour, aMinutes] = a.start.split(':');
  const [bHour, bMinutes] = b.start.split(':');
  if (aHour === bHour) {
    return parseInt(aMinutes) - parseInt(bMinutes);
  }
  return parseInt(aHour) - parseInt(bHour);
});


test('it sorts the meetings by interval', () => {
  expect(sortedMeetings[0].start).toBe('10:00');
  expect(sortedMeetings[1].start).toBe('11:50');
  expect(sortedMeetings[2].start).toBe('12:00');
  expect(sortedMeetings[3].start).toBe('19:00');
});

test('it checks if the meetings  overlap', () => {
  const isOverlap = sortedMeetings.some((meeting, index) => {
    if (index === 0) return false;
    const [aHour, aMinutes] = meeting.start.split(':');
    const [bHour, bMinutes] = sortedMeetings[index - 1].start.split(':');
    const aTime = parseInt(aHour) * 60 + parseInt(aMinutes);
    const bTime = parseInt(bHour) * 60 + parseInt(bMinutes);
    return aTime - bTime < sortedMeetings[index - 1].duration;
  });
  expect(isOverlap).toBe(true);
});

